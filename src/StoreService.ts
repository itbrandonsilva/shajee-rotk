import { Injectable } from '@angular/core';
import * as Redux from 'redux';

@Injectable()
export default class StoreService {
    store: any;
    state: any = {count: 0};

    constructor() {
        this.store = Redux.createStore((state = {count: 0}, action) => {
            switch (action.type) {
                case 'INC':
                    state.count++;
                    break;
                case 'DEC':
                    state.count--;
                    break;
            }

            return Object.assign({}, state);
        });

        this.store.subscribe(() => {
            this.state = this.store.getState();
        });
    }

    inc() {
        this.store.dispatch({type: 'INC'});
    }

    dec() {
        this.store.dispatch({type: 'DEC'});
    }
}
