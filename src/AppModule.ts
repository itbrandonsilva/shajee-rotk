import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './AppComponent.ts';
import ScrollingIcon from './ScrollingIcon.ts';

@NgModule({
    imports:      [ BrowserModule ],
    declarations: [ AppComponent, ScrollingIcon ],
    bootstrap:    [ AppComponent ]
})

export class AppModule { }
