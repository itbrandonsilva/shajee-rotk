import { Component, Input, HostListener } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import StoreService from './StoreService.ts';
import * as _ from 'lodash';

@Component({
    selector: 'scrolling-icon',
    styles: [
        '.button { float: left; background-color: teal; }',
        '.iconsContainer { position: relative; overflow: hidden; float: left; background-color: red; }',
    ],
    template: `
        <div class="button buttonLeft" [style]="_buttonStyle"></div>
        <div class="iconsContainer" [style]="_iconsContainerStyle">
            <img *ngFor="let icon of _icons" [src]="icon.url" [style]="icon.style" [width]="iconWidth" [height]="iconHeight"/>
        </div>
        <div class="button buttonRight" [style]="_buttonStyle" *ngIf="_enableSlider" (click)="shift()">
        </div>
    `
})
export default class ScrollingIcon {
    @Input() icons: Array<any>;
    @Input() iconWidth: number;
    @Input() iconHeight: number;
    @Input() transitionDuration: number = 0.25;
    private _icons: Array<any>;
    private _enableSlider: boolean;
    private _enableShift: boolean = true;
    private _sliderThreshold: number;
    private _buttonWidth: number;
    private _containerWidth: number;
    private _containerStart: number;
    private _iconMargin: number;
    private _maxViewableIcons: number;
    private _iconsContainerStyle: SafeStyle;
    private _buttonStyle: SafeStyle;

    constructor(private sanitizer: DomSanitizer) {
        this._enableSlider = true;
    }

    ngOnInit() {
        this.configure();
    }

    configure() {
        alert(window.innerWidth);
        this._buttonWidth = Math.min(window.innerWidth * 0.15, 130);
        this._containerWidth = window.innerWidth - (2 * this._buttonWidth);
        this._containerStart = this._buttonWidth;
        this._maxViewableIcons = Math.floor(window.innerWidth / (this.iconWidth + (0.3 * this.iconWidth)));
        this._iconMargin = ((this._containerWidth - (this._maxViewableIcons * this.iconWidth)) / this._maxViewableIcons);
        if ( ! this.iconHeight ) this.iconHeight = this.iconWidth;

        console.log(this._containerWidth);
        console.log(this._maxViewableIcons);
        console.log(this._iconMargin);

        let maxPosition;
        this._icons = this.icons.map((icon, idx) => {
            let cloned = _.clone(icon);
            cloned.left = 0;

            if ( ! maxPosition ) cloned.position = -this._iconMargin/2 - this.iconWidth;
            else                 cloned.position = maxPosition + this._iconMargin + this.iconWidth;
            maxPosition = cloned.position;

            cloned.style = this.generateStyle(cloned.left, cloned.position, false);
            return cloned;
        });

        this._sliderThreshold = maxPosition + this.iconWidth + this._iconMargin - 1;

        let heightStyle = `height: ${this.iconHeight}px;`;
        this._iconsContainerStyle = this.sanitizeStyle(`${heightStyle} width: ${this._containerWidth}px;`);
        this._buttonStyle = this.sanitizeStyle(`${heightStyle} width: ${this._buttonWidth}px;`);

        console.log(this._iconsContainerStyle);
        console.log(this._buttonStyle);
    }

    shift() {
        if ( ! this._enableShift ) return;
        this._enableShift = false;
        this._icons.forEach(icon => this.shiftIcon(icon));
        setTimeout(() => this._enableShift = true, this.transitionDuration * 1000);
    }

    shiftIcon(icon) {
        icon.position += this.iconWidth + this._iconMargin;
        icon.style = this.generateStyle(icon.left, icon.position, true);
        if (icon.position >= this._sliderThreshold) {
            setTimeout(() => {
                //icon.position = -this.iconWidth-this._iconMargin;
                icon.position = -((this._iconMargin/2) + this.iconWidth);
                console.log(icon.position);
                icon.style = this.generateStyle(icon.left, icon.position, false);
            }, this.transitionDuration * 1000);
        }
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        setTimeout(() => {
        this.configure();
        }, 1000);
    }

    sanitizeStyle(style: string): SafeStyle {
        return this.sanitizer.bypassSecurityTrustStyle(style);
    }

    generateStyle(start: number, end: number, animate?: boolean): SafeStyle {
        let position = `position: absolute;`;
        let tl = `top: 0px; left: ${start}px;`;
        let transformX = `transform: translateX(${end}px);`;
        let transition = animate ? `transition-property: transform; transition-duration: ${this.transitionDuration}s;` : '';

        let style = position + tl + transformX + transition;
        return this.sanitizeStyle(style);
    }
}

/*
let body = document.getElementsByTagName('body')[0];
let style = document.createElement('style');
body.appendChild(style);

function updateKeyframes() {
    style.innerText = `
        @keyframes scroll {
            0% {
                transform: translateX(0px);
            }
            99.99% {
                transform: translateX(${window.innerWidth}px);
            }
            100% {
                transform: translateX(0px);
            }
        }
    `;
}

updateKeyframes();
window.onresize = updateKeyframes;
*/
