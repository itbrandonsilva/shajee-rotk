import { Component } from '@angular/core';
import StoreService from './StoreService.ts';

@Component({
    selector: 'my-app',
    template: `
        <h1>My First Angular 2 App</h1>
        <h2>{{ store.state.count }}</h2>
        <button (click)="store.inc()">Inc</button>
        <button (click)="store.dec()">Dec</button>

        <br />
        <br />
        <scrolling-icon [icons]="icons" [iconWidth]="128"></scrolling-icon>
    `,
    providers: [StoreService]
})
export class AppComponent {
    private icons : Array<any>;
    constructor(private store: StoreService) {
        this.icons = [
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
            {
                url: 'https://chocolatey.org/content/packageimages/nodejs.6.7.0.png',
            },
        ];
    }
}
