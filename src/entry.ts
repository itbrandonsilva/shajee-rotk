import 'zone.js/dist/zone';
import 'reflect-metadata';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './AppModule.ts';

const platform = platformBrowserDynamic();

platform.bootstrapModule(AppModule);
