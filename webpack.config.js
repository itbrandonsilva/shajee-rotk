module.exports = {
    entry: './src/entry.ts',
    output: {
        filename: './public/bundle.js'
    },
    resolve: {
        extensions: ['', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
            },
            {
                test: /(\.scss$|\.css$)/,
                loaders: ['style', 'css', 'sass']
            }
        ]
    }
}
